const path = require('path');
const { AureliaPlugin } = require('aurelia-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: 'aurelia-bootstrapper',

    output: {
        path: path.resolve(__dirname, 'dist'),
        publicPath: '/',
    },

    resolve: {
        extensions: ['.ts', '.js'],
        modules: ['src', 'node_modules'].map(x => path.resolve(x)),
    },

    module: {
        rules: [
            // css/scss rules are in the dev/prod files
            {
                test: /\.ts$/,
                use: 'awesome-typescript-loader'
            }, {
                test: /\.html$/,
                use: 'html-loader'
            },
        ]
    },

    plugins: [
        new AureliaPlugin(),
        new HtmlWebpackPlugin({
            template: 'src/index.ejs',
        }),
    ],
};
